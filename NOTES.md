

The wp-cli program is pretty cool. 

It even has packages (plugins) that can be installed to augment core functionality. See `wp package`.



Missing commands from wpef that are in wpe


Basic Commands:
  changelog     Open the DevKit changelog in your browser.
  clone         Clone the specified install from the WP Engine servers.
  feedback      Report a bug or provide general feedback.
  login         Login to a local install using a one-time login url.


Development:
  genesis       Execute StudioPress Genesis commands.
  ngrok         Open or retrieve an ngrok url.

Configuration:
  keygen        Sets up a new SSH key for use in developing with WP Engine.


Alpha Commands:
  alpha cache     Open or retrieve the cached url.
  alpha delete    Deletes the specified project or the current project if none have been specified
  alpha generate  Generates template workflow configuration files for deployment to WP Engine with CI/CD tools.
  alpha memcache  Start/Stop memcached.
  alpha pull      Pull the WordPress site from WP Engine using rsync.
  alpha push      Push the WordPress site to the specified WP Engine install using rsync.
  alpha reset     Reset the local WPE Devkit environment
  alpha sites     List and manage your WP Engine sites.
  alpha tools     Open one of the included tools like phpMyAdmin.
  alpha xdebug    Start/Stop Xdebug and turn on/off listening for it.





Need `flyinghippo/devkit-cli` image that contains latest version of wpef. 

Here's the generated `.circleci/config.yml` when running `wpe generate circleci`
```
version: 2
jobs:
  build:
    docker:
      - image: wpengine/devkit-cli:latest

    steps:
      - checkout
      - run: echo "$INSTALL_NAME.ssh.wpengine.net ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDTbMliVDtORwqyYLhC9aRqtY/r6CTfTZc5FMua/HStjJ32UauebHaeEKFBFhMxDLEM0svPeu94O2+T1S4N6UXpm403fiZ2wy8SAA/VpePhKJZmRv5vTwErisPR3Da0iVt1uNFxjDswmezGiAiHNUF58qLZFdoMtpO4oZbZeA6ACid0FbPVE+ZTfICIdrSQELC6V7KBnKYLSO7W5bRGw5MfN3L36uSoYhbzPk5y0kSpL8H/W2rKSHFwVM5lLaHtPIufTVsXgBdCZhsqn1clmfDymgDN7KGfhhE7hlwMwCIU5htn1qkpiVW2iz2TDQ2gnyHYMF5/mamT50UT2KVt8EHD4mxvJqeL0DYqLOuoc+Vphcvja0xK/JLxwiC6N8JwAuIGsXQkJiD/CTdgWqYEWzT0V7xhSYKm5+X6Jdk2V477pq75M08VTr/xU/D+eBvAFe+EGssYWWpsfG5zvExUQGNsuBMFXK3hq4Kp/7+7JLY4MdSZDHE1hlRzAMJKvChkdV+y4/8w1jml4+GCl09X6ZIK7wgoAYSS3wZmVWRXEghKH5R2yr/qBRR8u2aKrw1hlJ0KVRJd6GzU7cgvHcSePXesndSfV1w6DLTnn7k23t4oxBoAZMRFYr7PR9QQ8fFtU33ylHNK5PREDvtQEvMizF0UISNLNlhYPOk3EFdSYxsgSQ==" >> ~/.ssh/known_hosts
      - run: wpe config set email $EMAIL_ADDRESS
      - run: wpe config set ssh_key ''
      - run: wpe config set analytics no
      - run: wpe config set update_check yes
      - run: mkdir .wpe-devkit
      - run: touch .wpe-devkit/config.yaml
      - run: wpe config set prod_install $INSTALL_NAME
      - run: wpe push -i $INSTALL_NAME -f
```


`wpe alpha xdebug webgrind` install the webgrind xdebug tool.


### wpe alpha xdebug --help

```
WP Engine DevKit xdebug command:
  Start/Stop Xdebug and turn on/off listening for it.

Description:
  Start/Stop Xdebug and turn on/off listening for it.

Usage:
  wpe alpha xdebug [flags]
  wpe alpha xdebug [command]

Commands:
  off           Disable xdebug.
  on            Enable xdebug.
  xloff         Disable remote_autostart.
  xlon          Enable remote_autostart.

Options:
  -h, --help    help for xdebug

Global Options:
  -c, --config  string Overide the application config file.
  -v, --verbose Enable verbose output.

Have more questions? Join our developer community using this special invite link:
https://developer.wpengine.com/register/?invitecode=devkitopenbeta2019

```

### wpe -v alpha xdebug on

```
2020/12/19 15:34:19 [docker-compose -f /Users/rparlee/Developer/wpengine/myproject1/.wpe-devkit/conf/docker/docker-compose.yml -p myproject1 exec -u root php bash -c xdctl -q --enable-xdebug]
2020/12/19 15:34:19 
runDockerCommand
  Args: [docker-compose -f /Users/rparlee/Developer/wpengine/myproject1/.wpe-devkit/conf/docker/docker-compose.yml -p myproject1 exec -u root php bash -c xdctl -q --enable-xdebug]
2020/12/19 15:34:20 stderr: No container found for php_1
Error: It looks like your site isn't running. Please try running wpe start first.
```

### wpe containers

docker image ls | grep wpengine | cut -d' ' -f1 | sort -u
wpengine/devkit_docker_proxy
wpengine/devkit_mailhog
wpengine/devkit_memcached
wpengine/devkit_mitmproxy
wpengine/devkit_mkcert
wpengine/devkit_mysql_5_7
wpengine/devkit_nginx
wpengine/devkit_php_7_3
wpengine/devkit_traefik
wpengine/devkit_varnish
wpengine/devkit_webgrind

###  wpe version

```
Version: 0.19.2
Git Commit Hash: 298af305a781e17700628e9bd55964e5caa0615f
UTC Build Time: 2020-09-15T21:22:24Z
```

