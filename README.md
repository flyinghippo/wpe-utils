# FH Devkit

Utility to support local WordPress site development.

Includes push/pull to sites hosted on WPEngine.

Should run on any POSIX-compatible operating system. 


## Table of Contents

- [Intro](#intro)
- [Installation](#installation)
- [Setup](#setup)
- [Usage](#usage)

## Intro

`wpef` makes local development of WordPress websites easy. 

**Example**
```bash
$ git clone git://gitlab.com/mysite.git
$ cd mysite
$ wpef init mysite
$ wpef start
```

See [usage](#usage) for more exmaples. 

## Dependencies

### Non-default on MacOS/Linux
- [jq](https://stedolan.github.io/jq/) (^1.5.1)
  
  MacOS
    ```
    brew install jq
    ```
    ```
    port install jq
    ```
  Ubuntu
    ```
    apt-get install jq
    ``` 
  Alpine Linux
    ```
    apk add jq
    ```
- [yq](https://mikefarah.gitbook.io/yq/) (^3.3.2 or 4+)
  
  MacOS
    ```
    brew install yq
    ```
    ```
    wget https://github.com/mikefarah/yq/releases/download/3.4.1/yq_darwin_amd64 -O /usr/local/bin/yq &&\
        chmod +x /usr/local/bin/yq
    ```
    ```
    port install yq
    ```
  Ubuntu 16.04+
    ```
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys CC86BB64
    sudo add-apt-repository ppa:rmescandon/yq
    sudo apt update
    sudo apt install yq -y
    ```
  Alpine Linux
    ```
    apk add yq
    ```
- docker
- docker-compose

### Probably already included on MacOS/Linux
- grep
- egrep
- sort
- curl
- basename
- dirname
- tr
- sed
- awk
- uname
- head
- tail
- bash (v3.2.58+)
- rm
- diff
- mv
- mkdir
- cat
- sleep
- printf
- rsync

## Installation

```bash
curl -sSL https://gitlab.com/parleer/wpe-utils/raw/latest/wpef -o wpef
chmod ugo+x wpef
sudo mv wpef /usr/local/bin
```

After installing `wpef`, run `wpef update check` to make sure that you're on the latest version. (i.e. In case I forgot to update the `latest` tag.)

### Windows

`wpef` now runs on Windows inside WSL-v2. Be sure to follow https://docs.docker.com/docker-for-windows/wsl/

Be sure to install [dependencies](#dependencies) for your version of Linux. WSL-v2 defaults to Ubuntu. 

Additionally, you'll need to install `php`:

```bash
sudo apt-get update
sudo apt-get install php
sudo apt-get install php-curl
sudo apt-get install php-xmlwriter
sudo apt-get install php-zip
```

Many limitations prevent this from being an efficient work environment at this time:
- Lack of step debugging
- Unable to automatically synchronize changes to /etc/hosts
- Inability to launch browser from terminal
- Permissions issues 
  - Unable to generate SSL certificates 
    - `sudo chmod ugo+w ~/.fh-devkit/certs/cert.pem`
  - Unable to remove local .fh-devkit because logs/ gets created as root 
  - Unable to `git submodule init && git submodule update`
    - `sudo usermod -a -G www-data ${USER}`
    - `sudo chmod -R g+w web/`


## Setup

Ensure that your SSH (`~/.ssh/id_rsa.pub`) key is added to https://my.wpengine.com/ssh_keys


## Usage

### Checkout a project and spin up a local container

```bash
git clone https://gitlab.com/www-example-com
cd www-example-com
wpef init example web
wpef config set prod_install exampleP
wpef db fetch -e prod
wpef start
```

### ~~Pull changes from WPEngine~~

*DO NOT USE `wpef pull`*

```bash
wpef config set prod_install exmapleP
wpef pull -e prod
```

### ~~Push changes to WPEngine~~

*DO NOT USE `wpef push`*

```bash
wpef config set stage_install exampleS
wpef push -e stage
```

### Get plugins or thems from WPEngine

```bash
wpef config set prod_install exmapleP
wpef get -e prod fh-*
```

### Put plugins or themes to WPEngine

```bash
wpef config set stage_install exampleS
wpef put -e stage fh-algolia hs-*
```



### Fetch database from WPEngine

```bash
wpef db fetch
```

### Import database file into local MySQL container

```bash
wpef db import 
```

Defaults to:

```bash
wpef db import -f db_init/autoload.sql
```

### Export local MySQL container database 

```bash
wpef db export
```

Defaults to:

```bash
wpef db export -f db_init/autoload.sql
```

## WordPress Multisite

Initialize using the `--multisite` option: 

```bash
wpef init --mulitsite example web
```

Then when you `wpef start`, the database will automatically be updated with the correct url(s). 


To manually adjust the database, run the `wpef multisite` utility. 


### How it works:

`wpef init --multisite` will set the `type` config setting to `wordpress-multisite` and append the following to `WORDPRESS_CONFIG_EXTRA: |` within `.fh-devkit/docker-compose.yml`:

```php
define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', $_SERVER['SERVER_NAME']);
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
```

`wpef start` on a `wordpress-multisite` installation will execute the following SQL:

```sql
update wp_site set domain = replace(domain, '{old_domain}', '{new_domain}');
update wp_sitemeta set meta_value = replace(meta_value, '{old_domain}', '{new_domain}') where meta_key = 'siteurl';
update wp_blogs set domain = replace(domain, '{old_domain}', '{new_domain}');
update wp_options set option_value = replace(option_value, '{old_domain}', '{new_domain}') where option_name in ('siteurl', 'home');
foreach blogid in blogids
  update wp_${blog_ids[$i]}_options set option_value = replace(option_value, '${old_domain}', '${new_domain}') where option_name in ('siteurl', 'home');
next
...
```

Use `wpef multisite` to execute above SQL on an already started container (i.e. in case `wpef start` failed to run because of a large autoload.sql file.)


## Xdebug

FH DevKit supports step debugging through [xdebug](https://xdebug.org/). 

**Requirements:** Make sure you are running v0.0.39+ of wpef and updated wpef-cli managed files. This will upgrade your `docker-compose.yml` to use `flyinghippo/devkit_%` images with enforced version tags. 

### Usage

```
  wpef xdebug [command]

Commands:
  off           Disable xdebug.
  on            Enable xdebug.
  xloff         Disable remote_autostart.
  xlon          Enable remote_autostart.
```

You can also check the status of xdebug with `wpef xdebug`:

```
$ wpef xdebug
XDebug: off
XDebug Listener: off
```


### Enable Step Debugging with VS Code

1. Install [PHP Debug](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug) by Felix Becker.

2. Configure `launch.json` with appropirate `pathMappings`. 

For example, if your WordPress files are in `web/`, your `launch.json` might look like this

<pre>
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Listen for XDebug",
            "type": "php",
            "request": "launch",
            "port": 9000, 
            <strong>"pathMappings": {
                "/var/www/html": "${workspaceRoot}"
            }</strong>
        },
        {
            "name": "Launch currently open script",
            "type": "php",
            "request": "launch",
            "program": "${file}",
            "cwd": "${fileDirname}",
            "port": 9000
        }
    ]
}
</pre>

3. Enable xdebug

```
wpef xdebug on
wpef xdebug xlon
```

4. Happy debugging


## Tips

1. Use wpef without wpe entirely! Just wpef init your project after cloning your git repo. To wpef push  you can setup defaults using `wpef config set prod_install <wpe_install_name>` or simply specify the install on the command line: `wpef push -i <install_name>`

1. Easily fetch a database from WPEngine using `wpef db fetch`. Again, you can setup defaults using `wpef config set prod_install` or simply specify the install on the command line: `wpef db fetch -i <install_name>`

1. No more getting behind on features! wpef now auto-checks for updates once per day. Of course you can always run `wpef update` to check manually.

1. wpef works from from any subdirectory. You’re in `/web/wp-content/themes/isu-master/functions`? No problem. Just run your favorite wpef command and it will automatically figure out what you’re talking about. Run `cd $(wpef root)` to switch to the root directory of your project.

1. Easily import a SQL file into your container’s database using `wpef db import`. wpef will try to automatically locate your SQL file (perhaps the one you just fetched using `wpef db fetch`) or you can specify it using the `-f filename` option.



## Misc

As part of the research into this project, I modified the GNU `diff` program to support exclusions of directories, similar to git-style exclusions. See https://github.com/parleer/diffutils

```
mydiff -Naur --exclude-directory -X .diff-ignore dirA dirB > dirB-to-dirA.diff
cd dirB
patch -u -p 1 -i ../dirB-to-dirA.diff 
```
