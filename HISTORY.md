
# HISTORY

This proejct started as a utility that fixed some glaring problems with WP Engine's WPE Devkit. However, WP Engine has announced that WPE DevKit will [no longer be supported](https://developer.wpengine.com/devkit/) and so this project has grown to become a complete replacement. 

## EVERYTHING BELOW IS LEGACY DOCUMENTATION AND NO LONGER APPLIES


The `wpe` command allowed developers to easy spin up local docker containers in order to test and debug their local code. It also allowed developers to push and pull to/from a live environment on WPEngine's servers. In order to work, the `wpe` command required a directory be properly configured as a *WPE Project*. But creating a *WPE Project* was problematic because `wpe` only supported project creation from either:

1. Cloning directly from WPEngine's live environments <br>(Not something you typically want to do if your code is source control managed.)

2. Creating a brand new project <br>(Not something you want to do if your code is already checked-in to source control management)

There was no ability to take code which you've just checked out from source control initialize it as a *WPE Project*.

`wpef` was originally created to solve this specific problem with `wpe`. 

`wpef` originally came with the following description:

*This script compliments WP Engine's [DevKit](https://developer.wpengine.com/devkit/) by providing a way to convert newly checked-out code into a *WPE Project*.*




Table of contents:

- [Installation](#user-content-installation-macos)
- [Migration to WPEngine](#user-content-migrate-an-existing-site-to-wpengine)
- [Local Development](#user-content-local-development)
- [WordPress Multisite](#user-content-wordpress-multisite)


Quick Links:
- [DevKit](https://wpengine.com/devkit/)
- [DevKit Support](https://wpengine.com/support/devkit/)
- [WPE DevKit Installation](https://developer.wpengine.com/devkit/installation.html)
- [Maximizing your development workflow with WP Engine DevKit](https://wpengine.com/wp-content/uploads/2019/06/WPE-EBK-LT-MaximizingYourDevelopmentWorkflow_v06.pdf)
- [WPE-CLI Commands](https://developer.wpengine.com/devkit/commands)
- [Deploying with DevKit](https://developer.wpengine.com/devkit/deploying)
- [Getting Started](https://developer.wpengine.com/devkit/getting_started.html)
- [FAQ](https://developer.wpengine.com/devkit/support/faq.html)
- [Getting Started with SSH Gateway](https://wpengine.com/support/getting-started-ssh-gateway/)
- [Migration Support](https://wpengine.com/support/migration-process/)
- [Migration Plugin](https://wpengine.com/support/wp-engine-automatic-migration-powered-by-blogvault/)
- [QueryMonitor](https://wordpress.org/plugins/query-monitor/)



## Installation (MacOS)


### Install Homebrew

Since I use MacPorts, I wanted to keep Homnebrew in a contained location used the [Alternative Installs](https://docs.brew.sh/Installation) procedure to install to `~/homebrew`
.

```
mkdir homebrew && curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C homebrew
```

Make sure to add the homebrew install location (`~/homebrew/bin`) to your `PATH`. 
```
echo 'export PATH="$PATH:~/homebrew/bin"' >> ~/.bash_profile
```

### Install wpe-cli

Install wpe-cli with Homebrew:

```
brew install wpengine/wpe-cli/wpe-cli
```

### Configure wpe-cli

```
wpe setup
```

1. Select NO when asked to setup the API key
2. Select NO when asked to setup an SSH key
3. Ensure that your SSH (`~/.ssh/id_rsa.pub`) key is added to https://my.wpengine.com/ssh_keys


### Install wpef

See https://gitlab.com/parleer/wpe-utils 

```
curl -sSL https://gitlab.com/parleer/wpe-utils/raw/latest/wpef -o wpef
chmod ugo+x wpef
sudo mv wpef /usr/local/bin
```


## Migrate an existing site to WPEngine

The easiest way to migrate a site is to use the WPEngine Migration Plugin. 

See https://wpengine.com/support/wp-engine-automatic-migration-powered-by-blogvault/




## Local Development

`wpe clone` allows you to *clone* your live environment to your local computer. However, since your live environment is not connected to source control, this workflow is cumbersome for real development **and should not be used**.

What a developer really wants to do is:

1. checkout source code from Git repo
2. somehow connect that source code to a WPEngine Live environment (db + assets)
3. spin up Local web server environment
4. commit code changes
5. push to a WPEngine live environment


This would look something like this:

<pre>
	<code>
# Checkout source code from Git repo
git clone {repo_url}

# Initialize the checked out code for use by wpe
cd {repo_name}
<strong>wpe init {local_project_name} {wordpress_website_dir}</strong>

# Connect to a Live environment
cd web
wpe config set prod_install {live_prod_environment}
wpe config set stage_install {live_stage_environment}

# Download copy of Live environment's database + handle wp-content
<strong>wpe db fetch</strong>
<strong>wpe redirect wp-content</strong>

# Startup Local web server environment
wpe start 

# Hack the code!
...

# Commit changes
git add -u
git commit -m '{message}'

# Publish changes to Staging environment
wpe push stage
</code>
</pre>


Unforutnately, `wpe` doesn't actually implement all of the above commands. There is *no way*, for example, to initialize an existing codebase for use by `wpe` -- you literally have to either 'clone' a live environment OR create a brand new project! So that's where the new `wpef` program helps:

### Instead of `wpe init {project_name}`

```
wpef init {project_name} {project_dir}
```

### Instead of `wpe db fetch`


```
wpef db fetch
```

### Instead of `wpe redirect wp-content`

TBD


## Other common things you may need to do with a WPE Environment


### Backup the current local database 

Backup the current local database to a location accessible by the local filesystem. Note, within the `php` docker container, the path `/wordpress` is bound to your local folder. A good location is to put the database somewhere in `_wpeprivate`  since this folder is ignored in wpe push and pull.

```
wpe wp db export /wordpress/_wpeprivate/autoload.sql
```


#### Import a database file into your local container

```
wpe wp db import _wpeprivate/autoload.sql
```


### Download the most recent backup of the live environment's database

```
WPE_ENV_NAME=$(wpe config get prod_install)
rsync --verbose --compress --times $WPE_ENV_NAME@$WPE_ENV_NAME.ssh.wpengine.net:/sites/$WPE_ENV_NAME/wp-content/mysql.sql _wpeprivate/autoload.sql
```
Or
```
wpe ssh -e prod 'cat wp-content/mysql.sql | gzip -c -' | gunzip -c > _wpeprivate/autoload.sql
```


### Create a new export of a live environment's database and download

```
wpe ssh -e prod 'wp --skip-plugins --skip-themes db export - | gzip -c -' | gunzip -c > _wpeprivate/autoload.sql
```

### Download wp-content/uploads

```
WPE_ENV_NAME=$(wpe config get prod_install)
rsync -r --verbose --compress --times $WPE_ENV_NAME@$WPE_ENV_NAME.ssh.wpengine.net:/sites/$WPE_ENV_NAME/wp-content/uploads/ wp-content/uploads
```



## WordPress Multisite

You will need to manually implement chnages to your MySQL database and wp-config.php in order to use `wpe start` with a WordPress Multsite database.


```
update wp_blogs set domain = 'DOMAIN.test' where blog_id != 0;
update wp_options set option_value = 'http://DOMAIN.test' where option_name = 'siteurl';
update wp_options set option_value = 'http://DOMAIN.test' where option_name = 'home';
```

wp-config.php

```

define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', getenv('DOMAIN_CURRENT_SITE'));
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
```

wp-config for SMTP plugin:
```
define('GF_LICENSE_KEY', '' );
define('GLOBAL_SMTP_HOST','');
define('GLOBAL_SMTP_USER','');
define('GLOBAL_SMTP_PASSWORD','');
define('GLOBAL_SMTP_FROM',''); // Defaults to Network Admin Email Address
define('GLOBAL_SMTP_FROM_NAME',''); // Defaults to Network Name
define('GLOBAL_SMTP_PORT',465);
define('GLOBAL_SMTP_SECURE;','tls');
define('GLOBAL_SMTP_DEBUG',true);
```



## Misc

As part of the research into this project, I modified the GNU `diff` program to support exclusions of directories, similar to git-style exclusions. See https://github.com/parleer/diffutils

```
mydiff -Naur --exclude-directory -X .diff-ignore dirA dirB > dirB-to-dirA.diff
cd dirB
patch -u -p 1 -i ../dirB-to-dirA.diff 
```
