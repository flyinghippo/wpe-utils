#!/bin/bash

code=(
	# SC2120 #: wpef_update references arguments, but none are ever passed.
	# SC2154 #: commands is referenced but not assigned.
	# SC2119 #: Use wpef_mysql "$@" if function's $1 should mean script's $1.
	# SC2030 #: Modification of ERR is local (to subshell caused by $(..) expansion).
	# SC2031 #: ERR was modified in a subshell. That change might be lost.
	# SC2002 #: Useless cat. Consider 'cmd < file | ..' or 'cmd file | ..' instead.
	# SC2103 #: Use a ( subshell ) to avoid having to cd back.
	# SC2034 #: accept_new_host_keys appears unused. Verify use (or export if used externally)
)
codes="$(printf '%s,' "${code[@]}")"
shellcheck --shell=bash --exclude="${codes%?}" wpef
