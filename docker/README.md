
# wpef/docker

This folder contains the various docker projects that support the `wpef` script. 

The root `Makefile` recursively calls subfolder Makefiles.

The root `Makefile` defaults `VERSION` to the `DOCKER_IMAGE_VERSION` variable declared in `../wpef`.


## Todo
- [ ] Need to build Intel based images in addition to linux/arm64


## Development Tasks

### Build all images

```bash
make -e VERSION=0.0.1 build  # build all images and tag with 0.0.1
```
```bash
make build  # build all images and tag with DOCKER_IMAGE_VERSION
```

### Tag all images

```bash
make -e VERSION=0.0.1 -e NEW_VERSION=latest tag  # tag images taggged with 0.0.1 as latest
```

```bash
make -e NEW_VERSION=latest tag  # tag images taggged with DOCKER_IMAGE_VERSION as latest
```

There's also a special target `tag-latest` that will automatically set `NEW_VERSION=latest`. 

```bash
make tag-latest  # tag images taggged with DOCKER_IMAGE_VERSION as latest
```

### Push all images to Docker Hub

```bash
make -e VERSION=0.0.1 push  # push images tagged with 0.0.1
```

```bash
make push  # push images tagged with DOCKER_IMAGE_VERSION
```

```bash
make -e VERSION=latest push  # push images tagged with latest
```


## Research

- https://web.archive.org/web/20200209034547/http://aegis.sourceforge.net/auug97.pdf
- http://abusalimov.blogspot.com/2009/12/yet-another-implementation-of-non.html
- https://stackoverflow.com/questions/9249757/recursive-make-in-subdirectories
- https://docs.docker.com/docker-hub/builds/
- https://docs.docker.com/develop/develop-images/dockerfile_best-practices/


## Multi-platform architectures

- https://www.docker.com/blog/how-to-rapidly-build-multi-architecture-images-with-buildx/
- https://medium.com/@artur.klauser/building-multi-architecture-docker-images-with-buildx-27d80f7e2408
- https://docs.docker.com/build/ci/github-actions/examples/#multi-platform-images


Before you can build multi-platform images, you need to create a custom builder. This is a one-time setup process. 

```bash
docker buildx create --name mybuilder
docker buildx use mybuilder
docker buildx inspect --bootstrap
```

```bash
$ docker pull --platform x86_64 alpine
$ docker image inspect alpine --format '{{.Os}}/{{.Architecture}}'
linux/amd64
$ docker pull --platform aarch64 alpine
$ docker image inspect alpine --format '{{.Os}}/{{.Architecture}}'
linux/arm64
```