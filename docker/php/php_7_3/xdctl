#!/bin/sh
set -eu

STATIC_INI="/var/xdebug/xdebug.ini"
STATIC_LINK=/usr/local/etc/php/conf.d/xdebug.ini
OUTPUT='HUMAN'
XDEBUG='CHECK'
XDEBUG_LISTEN='CHECK'
XDEBUG_LISTENER_HOST=''


_get() {
	augtool -At "PHP.lns incl ${STATIC_INI}" get "/files${STATIC_INI}/$1" | cut -sd'=' -f2 | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//'
}

_set() {
	augtool -At "PHP.lns incl ${STATIC_INI}" set "/files${STATIC_INI}/$1" "$2"
}


while [ $# -gt 0 ]; do
	case "${1}" in
	-h|--help)
		cat <<_EOF_
Usage: xdctl [ OPTIONS ]

xdctl interacts with the XDebug settings in the PHP image of DevKit.

Available options:
	-h, --help
		Print this message.

	-q, --quiet
		Don't print the XDebug settings.

	--json
		Print the XDebug settings in JSON format.

	--enable-xdebug
		Enables XDebug.

	--disable-xdebug
		Disables XDebug.

	--enable-xdebug-listener
		Enables XDebug listening.

	--disable-xdebug-listener
		Disables XDebug listening.

	--enable-xdebug-trigger
		Enables XDebug trigger.

	--disable-xdebug-trigger
		Disables XDebug trigger.

	--set-xdebug-listener-host HOSTNAME
		Set the host for the XDebug listener. Unchanged if empty.
		Default: ${XDEBUG_LISTENER_HOST}

	--ini-file PATH
		Path to the XDebug INI file to check.
		Default: ${STATIC_INI}

		NOTE: This only works for status checks; the current
		enable/disable scripts use their own notion of the location of
		the XDebug INI.

	--debug
		Enable debugging output of this script.

_EOF_
		exit 0
		;;
	-q|--quiet)
		OUTPUT='QUIET'
		;;
	--json)
		OUTPUT='JSON'
		;;
	--enable-xdebug)
		XDEBUG='on'
		;;
	--disable-xdebug)
		XDEBUG='off'
		;;
	--enable-xdebug-listener)
		XDEBUG_LISTEN='on'
		;;
	--disable-xdebug-listener)
		XDEBUG_LISTEN='off'
		;;
	--enable-xdebug-trigger)
		XDEBUG_LISTEN='trigger'
		;;
	--disable-xdebug-trigger)
		XDEBUG_LISTEN='off'
		;;
	--set-xdebug-listener-host)
		shift
		XDEBUG_LISTENER_HOST="${1}"
		shift
		;;
	--ini-file)
		shift
		DYNAMIC_INI="${1}"
		shift
		;;
	--debug)
		set -x
		;;
	config)
		shift
		if [ ! -f "${STATIC_INI}" ]; then
			echo "Error: XDebug INI file does not exist: ${STATIC_INI}" >&2
			exit 1
		fi
		while [ $# -gt 0 ]; do
			case "${1}" in
				list|ls)
					shift
					augtool -At "PHP.lns incl ${STATIC_INI}" print "/files${STATIC_INI}/XDEBUG" | grep XDEBUG/xdebug | sed -e 's/.*XDEBUG\/xdebug\.//' | sort
					exit
					;;
				get)
					shift
					_get "XDEBUG/xdebug.${1}"
					exit
					;;
				set)
					shift
					_set "XDEBUG/xdebug.${1}" "${2}"
					exit
					;;
				*)
					echo "Error: Unknown option: $1" >&2
					printf "\nSee xdctl --help\n" >&2
					exit 1
			esac
			shift
		done
		;;
	*)
		echo "Error: Unknown option: $1" >&2
		printf "\nSee xdctl --help\n" >&2
		exit 1
	esac
	shift
done


if [ ! -f "${STATIC_INI}" ]; then
	echo "Error: XDebug INI file does not exist: ${STATIC_INI}" >&2
	exit 1
fi

if [ "${XDEBUG}" = 'on' ] && [ ! -L "${STATIC_LINK}" ]; then
	ln -nsf "${STATIC_INI}" "${STATIC_LINK}"
elif [ "${XDEBUG}" = 'off' ] && [ -L "${STATIC_LINK}" ]; then
	rm "${STATIC_LINK}"
fi

if [ "${XDEBUG_LISTEN}" = 'on' ]; then
	_set "XDEBUG/xdebug.start_with_request" "yes"
elif [ "${XDEBUG_LISTEN}" = 'trigger' ]; then
	_set "XDEBUG/xdebug.start_with_request" "trigger"
elif [ "${XDEBUG_LISTEN}" = 'off' ]; then
	_set "XDEBUG/xdebug.start_with_request" "no"
fi

if [ -n "${XDEBUG_LISTENER_HOST}" ]; then
	_set "XDEBUG/xdebug.client_host" "${XDEBUG_LISTENER_HOST}"
fi

# Print status 
if [ -L "${STATIC_LINK}" ] && [ -n "$(_get ".anon/zend_extension")" ]; then
	XDEBUG='on'
else
	XDEBUG='off'
fi

start_with_request=$(_get "XDEBUG/xdebug.start_with_request")
if [ -L "${STATIC_LINK}" ] && [ "$start_with_request" = 'yes' ]; then
	XDEBUG_LISTEN='on'
elif [ -L "${STATIC_LINK}" ] && [ "$start_with_request" = 'trigger' ]; then
	XDEBUG_LISTEN='trigger'
else
	XDEBUG_LISTEN='off'
fi

# Output (or not) in whatever format
if [ "${OUTPUT}" = 'HUMAN' ]; then
	cat <<-_EOF_
	XDebug: ${XDEBUG}
	XDebug Listener: ${XDEBUG_LISTEN}
	_EOF_
elif [ "${OUTPUT}" = 'JSON' ]; then
	printf '{"xdebug":'
	if [ "${XDEBUG}" = 'on' ]; then
		printf 'true'
	else
		printf 'false'
	fi
	printf ',"xdebug-listen":'
	if [ "${XDEBUG_LISTEN}" = 'on' ]; then
		printf 'true'
	elif [ "${XDEBUG_LISTEN}" = 'trigger' ]; then
		printf '"trigger"'
	else
		printf 'false'
	fi
	printf '}\n'
fi
