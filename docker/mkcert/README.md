# flyinghippo/devkit_mkcert

```bash
docker build -t flyinghippo/devkit_mkcert .
```


Dockerfile Inspiration:

- wpengine/devkit_mkcert
- https://hub.docker.com/r/vishnunair/docker-mkcert/dockerfile
- https://github.com/containous/whoami

A Dockerfile that creates a minimal container is cool, but doesn't work for our purposes because we need `bash` and ohter commands to execute our mkcert.sh script

```docker
FROM golang:1-alpine as builder

RUN apk --no-cache --no-progress add git ca-certificates tzdata make \
    && update-ca-certificates \
    && rm -rf /var/cache/apk/*

RUN go get -u github.com/FiloSottile/mkcert

#RUN go get -u github.com/FiloSottile/mkcert && \
#    cd src/github.com/FiloSottile/mkcert && \
#    go build -o /bin/mkcert

#WORKDIR /go/whoami

# Download go modules
#COPY go.mod .
#COPY go.sum .
#RUN GO111MODULE=on GOPROXY=https://proxy.golang.org go mod download

#COPY . .

#RUN make build

# Create a minimal container to run a Golang static binary
FROM scratch

#COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
#COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
#COPY --from=builder /go/whoami/whoami .

COPY --from=builder /go/bin/mkcert .

#ENTRYPOINT ["/whoami"]
#EXPOSE 80
#CMD mkcert -install && for i in $(echo $domain | sed "s/,/ /g"); do mkcert $i; done && tail -f -n0 /etc/hosts


COPY mkcert.sh /usr/bin/mkcert.sh

CMD /bin/sh -c mkcert.sh
```


```docker
FROM golang:1-alpine

RUN apk --no-cache --no-progress add git ca-certificates tzdata make \
    && update-ca-certificates \
    && rm -rf /var/cache/apk/*

RUN go get -u github.com/FiloSottile/mkcert

COPY mkcert.sh /usr/bin/mkcert.sh

CMD /bin/sh -c mkcert.sh
```