#!/usr/bin/env bash

mkcert -install

root_ca=/root/.local/share/mkcert/rootCA.pem


wait_file() {
  local file="$1"; shift
  local wait_seconds="${1:-10}"; shift # 10 seconds as default timeout

  until test $((wait_seconds--)) -eq 0 -o -f "$file" ; do sleep 1; done

  ((++wait_seconds))

  return $wait_seconds
}

wait_file "$root_ca" && {
  echo "Unable to create root CA."
  exit 1
}

mkcert -key-file key.pem  --cert-file cert.pem $(echo $domains | sed "s/,/ /g")
