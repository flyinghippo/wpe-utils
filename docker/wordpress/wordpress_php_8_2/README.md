
# flyinghippo/devkit_wordpress


Don't confuse this container with [flyinghippo/wordpress](https://hub.docker.com/r/flyinghippo/wordpress) which is connected to [parleer/docker-wordpress](https://github.com/parleer/docker-wordpress). flyinghippo/wordpress is the old container used by Flying Hippo prior to `wpef`. It was used in the varius `launch-*.sh` scripts that performed `docker run` commands. 


```
docker build -t flyinghippo/devkit_wordpress .
```


As of 2020-dec-17, our wordpress container is based on `wordpress:latest` and our wp-cli container is based on `wordpress:cli` [[dockerhub]](https://hub.docker.com/_/wordpress), which runs on Alpine. [[github]](https://github.com/docker-library/wordpress/tree/c0d11ed412fef07e748a5463041b7be0b5755dd6/php7.4/cli). It may be useful, however, to also have the wp-cli installed on the container actually hosting our Wordpress install:


1. This is a minimal container, so we need to install some pre-requisites:

```
apt-get install dialog less
```

2. Install wp-cli in wordpress container [[1]](https://wp-cli.org/#installing)

```
curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
chmod +x wp-cli.phar
mv wp-cli.phar /usr/local/bin/wp
```

3. Setup `wp` as an alias with the `--allow-root` flag 

```
echo "alias wp='wp --allow-root'" > /root/.bashrc
```

## ToDo

- xdctl: support setting `mode` => [ `off`, `develop`, `coverage`, `debug`, `gcstats`, `profile`, `trace` ] or any combination

- xdctl: support setting `start_with_request` = `yes`, `no`, `trigger` 
