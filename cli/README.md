# flyinghippo/devkit_cli

A cli of the wpef script has little value. wpef needs directl access to your host macine in 
order to orchestrate the docker engine. Perhaps there's a way to have docker conainers
communicate with the host docker engine, but I don't know how to do that yet. The only 
reason I can think of to run wpef inside a container is to perhaps test setup and 
configuration commands, such as `setup`, `keygen`, `init`, `config`, `update`, `version`.



## Build the container

```
make build
```

## Run the container

```
make shell
```

## Push to dockerhub

```
make push
```

