# WordPress Multisite

- https://wordpress.stackexchange.com/questions/251116/how-to-use-wordpress-multisite-with-different-domain-names
- https://wordpress.org/support/article/before-you-create-a-network/
- https://wordpress.org/support/article/create-a-network/
- https://wpengine.com/support/what-is-wordpress-multisite/
- https://premium.wpmudev.org/blog/domain-mapping-wordpress-multisite/


## Network Setup

wp-config.php

```
define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', \$\$_SERVER['SERVER_NAME']); 
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);
```

```
define('COOKIE_DOMAIN', $_SERVER['HTTP_HOST']);
```

.htaccess

```
RewriteEngine On
RewriteBase /
RewriteRule ^index\.php$ - [L]

# add a trailing slash to /wp-admin
RewriteRule ^wp-admin$ wp-admin/ [R=301,L]

RewriteCond %{REQUEST_FILENAME} -f [OR]
RewriteCond %{REQUEST_FILENAME} -d
RewriteRule ^ - [L]
RewriteRule ^(wp-(content|admin|includes).*) $1 [L]
RewriteRule ^(.*\.php)$ $1 [L]
RewriteRule . index.php [L]

```

## Database Setup

There are several places in the database where a value contains a fully-qualfiied domain.


### wp_site
```
update wp_site set domain = 'domain.com' where path = '/' (or where id = 1?)
```

### wp_sitemeta
```
update wp_sitemeta set meta_value = 'http://domain.com' where meta_key = 'siteurl'
```

### wp_usermeta

The `wp_users` and `wp_usermeta` tables are shared across a network.


Users are granted to a subsite via entries in `wp_usermeta` 

| meta_key | meta_value | description |
| -------- | ---------- | ----------- |
| `wp_{ID}_capabilities` | `a:1:{s:13:"administrator";b:1;}` | PHP serialized array |
| `wp_{ID}_user_level` | 10 | Level |
| `primary_blog` | 3 | ID from wp_blogs |
| `source_domain` | hsisu-stg.us-east-1.elasticbeanstalk.com | |



### wp_blogs

| blog_id | site_id | domain | path |
| ------- | ------- | ------ | ---- |
| 1 | 1 | localhost:123 | / |

```
update wp_blogs set domain = 'localhost:N'
```

```
INSERT INTO wp_blogs VALUES
(1,1,'cohs.sites.flyinghippo.com','/','2018-11-02 20:50:56','2020-02-20 15:47:20',0,0,0,0,0,0),
(3,1,'kinesiology.cohs.sites.flyinghippo.com','/','2018-11-30 19:03:16','2020-02-20 15:36:48',0,0,0,0,0,0),
(4,1,'education.cohs.sites.flyinghippo.com','/','2018-11-30 19:03:42','2020-02-10 18:14:08',0,0,0,0,0,0),
(5,1,'aeshm.cohs.sites.flyinghippo.com','/','2018-11-30 19:04:11','2020-02-10 18:15:13',0,0,0,0,0,0),
(6,1,'fshn.cohs.sites.flyinghippo.com','/','2018-11-30 19:04:41','2020-02-10 18:17:59',0,0,0,0,0,0),
(7,1,'hdfs.cohs.sites.flyinghippo.com','/','2018-11-30 19:05:06','2020-02-10 18:32:11',0,0,0,0,0,0);
```

### wp_*_options
```
foreach ID in wp_blogs.ID 

	if ID == 1 then 
		tbl = "wp_options"
	else
		tbl = "wp_{ID}_options"
	fi

	update $tbl set option_value = 'http://localhost:N' where option_name = 'siteurl'
	update $tbl set option_value = 'http://localhost:N' where option_name = 'home'

next
```







